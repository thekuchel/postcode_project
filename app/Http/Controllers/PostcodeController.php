<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Postcode;

class PostcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(!is_null($request->input('postcode_no')))
          $data = $this->get_list_post_code_with_radius($request->input('postcode_no'), $request->input('radius'));

        return view('postcode.postcode-view',[
          'data' => $data,
          'input' => $request->input(),
          'is_error' => is_array($data) ? false : true //to show at the view , the result is available or not
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /* get list post code with distance */
    public function get_list_post_code_with_radius($post_code, $radius){
      $sqlquery = Postcode::where('latitude','<>', 0)
      ->where('longitude', '<>', 0);

      if (is_numeric($post_code)) {
    		$sqlquery->where("postcode",$post_code);
    	} else {
    		$sqlquery->where("suburb", "LOWER('$post_code')");
    	}
    	$get_post_code = $sqlquery->first();
    	$num =  $sqlquery->count();

    	if($num == 0){
    		return "Your Post Code / Suburb is invalid";
    	}

      /* this is formula for search distance */
    	$get_radius = Postcode::selectRaw("*, ROUND(
    		(
    			acos(
    				sin({$get_post_code->latitude} * pi()/180) * sin(latitude * pi()/180) + (cos({$get_post_code->latitude} * pi()/180) * cos(latitude * pi()/180) * cos(({$get_post_code->longitude} - longitude) * pi()/180))
    			) / (pi()/180)
    		) * 111.325
    	, 2) distance")
      ->where('latitude','<>', 0)
      ->where('longitude', '<>', 0)
      ->havingRaw('distance <= '. $radius)
      ->orderByRaw('distance')
      ->get()
      ->toArray();

    	if (count($get_radius) > 0) {
        return ['radius_data' => $get_radius, 'postcode_data' => $get_post_code];
    	} else {
    		return "No Result";
    	}
    }

}
