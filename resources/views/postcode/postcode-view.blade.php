<!DOCTYPE html>
<html>
  <head>
    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>
  </head>
  <body>
    <h3>POST CODE SEARCH</h3>
    <form action="" method='GET'>
      <table>
        <tr>
          <td>Postcode Number</td>
          <td>: <input type='text' name='postcode_no' value='{{ !is_null(old('postcode_no')) ? old('postcode_no') : (isset($input['postcode_no']) ? $input['postcode_no'] : '') }}'></td>
        </tr>
        <tr>
          <td>Show Other Postcode Within Radius</td>
          <td>: <input type='text' name='radius' value='{{ !is_null(old('radius')) ? old('radius') : (isset($input['radius']) ? $input['radius'] : '') }}'> KM</td>
        </tr>
        <tr>
          <td colspan="2"><input type='submit' value='Search'></td>
        </tr>
      </table>
    </form>
    @if($is_error)
      <h3 style='color:red'>{{$data}}</h3>
    @endif
    <div id="map"></div>
    <script>
      function initMap() {

        @if(is_array($data) && isset($data['postcode_data']))

          var current_loc = {lat: {{$data['postcode_data']->latitude}}, lng: {{$data['postcode_data']->longitude}}};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: current_loc
          });
        @else
          var uluru = {lat: -25.363, lng: 131.044};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 9,
            center: uluru
          });
        @endif

        @if(!$is_error && isset($data['radius_data']))
          var infowindow = new google.maps.InfoWindow();
          @foreach($data['radius_data'] as $idx => $loc)

            var marker_{{$idx}} = new google.maps.Marker({
              position: {lat: {{$loc['latitude']}}, lng: {{$loc['longitude']}}},
              map: map,
              title: '{{$loc['suburb']}}'
            });


            google.maps.event.addListener(marker_{{$idx}}, 'click', function() {
               infowindow.setContent("Suburb : {{$loc['suburb']}} , Postcode : {{$loc['postcode']}}");
               infowindow.open(map,marker_{{$idx}});
             });


          @endforeach
        @endif
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL73DgOCwxF_Ii2UM2Zg0soXklLR_YALw&callback=initMap">
    </script>
  </body>
</html>
